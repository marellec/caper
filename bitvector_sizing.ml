(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Sizing functions for bit-vector expressions
*)

open Bitvector

(*Propagated width information through an expression*)
let rec set_width_of_bv_exp (w : width) (bv_e : bv_exp) : bv_exp =
  match bv_e with
  | Hole (None, s) -> Hole (Some w, s)
  | Const (None, bv_e) -> Const (Some w, bv_e)
  | Concat (None, bv_e1, bv_e2) ->
      (*When concatting, we expect to already know the widths of what's being
        concatted*)
      let (bv_e1', Some bv_e1_w) = width_of_bv_exp bv_e1 in
      let (bv_e2', Some bv_e2_w) = width_of_bv_exp bv_e2 in
      assert (w = bv_e1_w + bv_e2_w);
      Concat (Some w, bv_e1', bv_e2')
  | Extract (_, w', _) ->
      assert (w' = w);
      bv_e
  | BNot (None, bv_e') ->
      BNot (Some w, set_width_of_bv_exp w bv_e')
  | BNegate (None, bv_e') ->
      BNegate (Some w, set_width_of_bv_exp w bv_e')
  | BAnd (None, bv_e1, bv_e2) ->
      BAnd (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BOr (None, bv_e1, bv_e2) ->
      BOr (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BXor (None, bv_e1, bv_e2) ->
      BXor (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BPlus (None, bv_e1, bv_e2) ->
      BPlus (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMul (None, bv_e1, bv_e2) ->
      BMul (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BDiv (None, bv_e1, bv_e2) ->
      BDiv (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BRem (None, bv_e1, bv_e2) ->
      BRem (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMod (None, bv_e1, bv_e2) ->
      BMod (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMinus (None, bv_e1, bv_e2) ->
      BMinus (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | ShLeft (None, bv_e1, bv_e2) ->
      ShLeft (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | LShRight (None, bv_e1, bv_e2) ->
      LShRight (Some w, set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | ZeroExtend  (None, o, bv_e') ->
      assert (w > o);
      ZeroExtend (Some w, o, set_width_of_bv_exp (w - o) bv_e')

  | Hole (Some w', _)
  | Const (Some w', _)
  | Concat (Some w', _, _) ->
      assert(w = w');
      bv_e
  | BNot (Some w', bv_e') ->
      assert(w = w');
      BNot (Some w', set_width_of_bv_exp w bv_e')
  | BNegate (Some w', bv_e') ->
      assert(w = w');
      BNot (Some w', set_width_of_bv_exp w bv_e')
  | BAnd (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BAnd (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BOr (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BOr (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BXor (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BXor (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BPlus (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BPlus (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMul (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BMul (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BDiv (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BDiv (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BRem (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BRem (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMod (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BMod (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | BMinus (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      BMinus (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | ShLeft (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      ShLeft (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | LShRight (Some w', bv_e1, bv_e2) ->
      assert(w = w');
      LShRight (Some w', set_width_of_bv_exp w bv_e1, set_width_of_bv_exp w bv_e2)
  | ZeroExtend (Some w', o, bv_e') ->
      assert(w = w');
      ZeroExtend (Some w', o, set_width_of_bv_exp (w - o) bv_e')

(*If width is given then we check it, otherwise we try to determine it
  (and check that it's consistent)*)
and width_of_bv_exp
 ?emit_explanation:(emit_explanation = !Config.diagnostic_output_mode)
 (bv_e : bv_exp) : bv_exp * width option =
  let width_of_mono (w_opt, bv_e') f =
    begin
      match w_opt with
      | None ->
          let (bv_e'', bv_e''_w_opt) = width_of_bv_exp bv_e' in
          (f (bv_e''_w_opt, bv_e''), bv_e''_w_opt)
      | Some w ->
          let bv_e'' = set_width_of_bv_exp w bv_e' in
          (f (w_opt, bv_e''), w_opt)
    end in
  let width_of_binary (w_opt, bv_e1, bv_e2) f =
      begin
        match w_opt with
        | None ->
            let (bv_e1', bv_e1_w_opt) = width_of_bv_exp bv_e1 in
            let (bv_e2', bv_e2_w_opt) = width_of_bv_exp bv_e2 in
            begin
              match bv_e1_w_opt, bv_e2_w_opt with
              | None, None ->
                (f (None, bv_e1', bv_e2'), None)
              | Some w, None ->
                let bv_e2'' = set_width_of_bv_exp w bv_e2' in
                (f (Some w, bv_e1', bv_e2''), Some w)
              | None, Some w ->
                let bv_e1'' = set_width_of_bv_exp w bv_e1' in
                (f (Some w, bv_e1'', bv_e2'), Some w)
              | Some w1, Some w2 ->
                let bv_e1'', bv_e2'' =
                  if w1 = w2 then
                    (bv_e1', bv_e2')
                  else
                    begin
                      if emit_explanation then
                        (*FIXME use Aux.error_output*)
                        Printf.eprintf "Width mismatch: %d vs %d\n* %s\n* %s\n" w1 w2 (string_of_bv_exp bv_e1') (string_of_bv_exp bv_e2');
                      if w1 > w2 then
                        (bv_e1', extend_bv (w1 - w2) bv_e2')
                      else (extend_bv (w2 - w1) bv_e1', bv_e2')
                    end in
                (f (Some w1, bv_e1'', bv_e2''), Some w1)
            end
        | Some w ->
            let bv_e1' = set_width_of_bv_exp w bv_e1 in
            let bv_e2' = set_width_of_bv_exp w bv_e2 in
            (f (w_opt, bv_e1', bv_e2'), w_opt)
      end in

  match bv_e with
  | Hole (w, _)
  | Const (w, _) -> (bv_e, w)
  | BNot (w_opt, bv_e') -> width_of_mono (w_opt, bv_e') (fun (x1, x2) -> BNot (x1, x2))
  | BNegate (w_opt, bv_e') -> width_of_mono (w_opt, bv_e') (fun (x1, x2) -> BNegate (x1, x2))
  | BAnd (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BAnd (w_opt, bv_e1, bv_e2))
  | BOr (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BOr (w_opt, bv_e1, bv_e2))
  | BXor (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BXor (w_opt, bv_e1, bv_e2))
  | BPlus (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BPlus (w_opt, bv_e1, bv_e2))
  | BMul (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BMul (w_opt, bv_e1, bv_e2))
  | BDiv (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BDiv (w_opt, bv_e1, bv_e2))
  | BRem (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BRem (w_opt, bv_e1, bv_e2))
  | BMod (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BMod (w_opt, bv_e1, bv_e2))
  | BMinus (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> BMinus (w_opt, bv_e1, bv_e2))
  | ShLeft (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> ShLeft (w_opt, bv_e1, bv_e2))
  | LShRight (w_opt, bv_e1, bv_e2) -> width_of_binary (w_opt, bv_e1, bv_e2) (fun (w_opt, bv_e1, bv_e2) -> LShRight (w_opt, bv_e1, bv_e2))
(*
  | ZeroExtend  (o, bv_e') ->
      o + width_of_bv_exp bv_e'
*)

and extend_bv (extension : width) (bv_e : bv_exp) : bv_exp =
  let bv_e', w_opt = width_of_bv_exp bv_e in
  let ew_opt =
    match w_opt with
    | None -> None
    | Some w -> Some (extension + w) in
  ZeroExtend (ew_opt, extension, bv_e')

let ensure_width (sought_width : width) (bv_e : bv_exp) : bv_exp =
  let bv_e', w_opt = width_of_bv_exp bv_e in
  match w_opt with
  | None ->
      failwith (Aux.error_output_prefix ^ ": " ^ "ensure_width: cannot size the width of bv_e"(*FIXME make more informative*))
      (*FIXME emit warning
      bv_e*)
  | Some w ->
      if w = sought_width then bv_e'
      else if w > sought_width then
        failwith (Aux.error_output_prefix ^ ": " ^ "ensure_width: w > sought_width"(*FIXME make more informative*))
      else extend_bv (sought_width - w) bv_e'

let infer_width_rel (bv_rel : bv_rel) : bv_rel =
  let equate_widths ((bv_e1, bv_e2) : (bv_exp * bv_exp))
   (f : (bv_exp * bv_exp) -> bv_rel) : bv_rel =
    match width_of_bv_exp bv_e1, width_of_bv_exp bv_e2 with
    | ((_, None), (_, None)) ->
        failwith (Aux.error_output_prefix ^ ": " ^ "Could not size relation's expressions: " ^
        string_of_bv_rel bv_rel)
    | ((bv_e1', Some w), (bv_e2', None)) ->
        f (bv_e1', set_width_of_bv_exp w bv_e2')
    | ((bv_e1', None), (bv_e2', Some w)) ->
        f (set_width_of_bv_exp w bv_e1', bv_e2')
    | ((bv_e1', Some w1), (bv_e2', Some w2)) ->
        let bv_e1'', bv_e2'' =
          if w1 = w2 then
            (bv_e1', bv_e2')
          else if w1 > w2 then
            (bv_e1', extend_bv (w1 - w2) bv_e2')
          else
            (extend_bv (w2 - w1) bv_e1', bv_e2') in
        f (bv_e1', bv_e2') in
  match bv_rel with
  | Eq (bv_e1, bv_e2) -> equate_widths (bv_e1, bv_e2) (fun (x1, x2) -> Eq (x1, x2))
  | ULt (bv_e1, bv_e2) -> equate_widths (bv_e1, bv_e2) (fun (x1, x2) -> ULt (x1, x2))
  | ULeq (bv_e1, bv_e2) -> equate_widths (bv_e1, bv_e2) (fun (x1, x2) -> ULeq (x1, x2))
  | UGt (bv_e1, bv_e2) -> equate_widths (bv_e1, bv_e2) (fun (x1, x2) -> UGt (x1, x2))
  | UGeq (bv_e1, bv_e2) -> equate_widths (bv_e1, bv_e2) (fun (x1, x2) -> UGeq (x1, x2))

let rec infer_width_formula (bv_formula : bv_formula) : bv_formula =
  match bv_formula with
  | Atom bv_rel -> Atom (infer_width_rel bv_rel)
  | And bv_formulas -> And (List.map infer_width_formula bv_formulas)
  | Or bv_formulas -> Or (List.map infer_width_formula bv_formulas)
  | Not bv_formula' -> Not (infer_width_formula bv_formula')
  | True
  | False -> bv_formula
