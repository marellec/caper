#!/bin/bash
#
# Test consists of checking that expansion in Caper is idempotent.
# Nik Sultana, March 2023.
#
# NOTE error output from Caper is sent to /dev/null

CAPER="$1"
QUERY="$2"
EXPECTATION="$3" # NOTE we ignore the expectation. Instead we expand the query, then expand the result, and compare that expansion with the result.
TEST_ID="I$4" # NOTE we add the "I" prefix.

if [[ "${CAPER}" = "" || "${QUERY}" = "" || "${EXPECTATION}" = "" ]] # NOTE this requires EXPECTATION since we reuse tests from the disambiguation test.
then
  echo "Need to specify path to Caper, a query, and the expected result." >&2
  exit 1
fi

TEMP=TEMP_${RANDOM}
# FIXME check that ${TEMP} doesn't exist

RESULT=`echo "${QUERY}" | ${CAPER} 2> ${TEMP}`

ERROR_OUT=`cat ${TEMP}`
rm ${TEMP}
if [[ "${ERROR_OUT}" != "" ]]
then
  ERROR_OUT=". stderr output: '${ERROR_OUT}'"
fi

EXPECTATION=`echo "${RESULT}" | ${CAPER} 2> ${TEMP}` # NOTE we overwrite EXPECTATION with the expansion of RESULT.
# FIXME the ERROR_OUT doesn't report on stderr output that took place while evaluating EXPECTATION
rm ${TEMP}

# FIXME in event the EXPECTATION matches RESULT, check anyway if ERROR_OUT is not empty, in which case show them to user.
[[ "${EXPECTATION}" = "${RESULT}" ]] && echo "${TEST_ID}: OK : ${QUERY}${ERROR_OUT}" || echo "${TEST_ID}: FAIL (${QUERY}) : Expected '${EXPECTATION}' but got '${RESULT}'${ERROR_OUT}"
