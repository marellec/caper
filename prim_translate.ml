(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Translating pcap primitives into bit-vector logic
*)

open Pcap_syntax

(*Incremented by the likes of VLAN*)
let packet_offset = ref 0

let ipv4_is_zero_fragment =
  let t =
    Bitvector.Eq
      (Bitvector.BAnd (Some Bitvector.bpf_size_halfword,
         Bitvector.Hole (Some Bitvector.bpf_size_halfword,
            "(packet_proj_16 (_ bv" ^
            string_of_int (20 + !packet_offset) ^
            " 32))"),
         Bitvector.Const (Some Bitvector.bpf_size_halfword, 0x1fff)),
       Bitvector.Const (Some Bitvector.bpf_size_halfword, 0)) in
  Bitvector.Atom t

let packet_offset_tcp (tcp_offset : Bitvector.bv_exp) : Bitvector.bv_exp =
    let ipv4_ihl_value =
      Bitvector.BAnd (Some Bitvector.bpf_size_byte,
        Bitvector.Hole (Some Bitvector.bpf_size_byte,
           "(packet_proj_8 (_ bv" ^
           string_of_int (14 + !packet_offset) ^
           " 32))"),
        Bitvector.Const (Some Bitvector.bpf_size_byte, 0xf)) in

    let ipv4_payload_start_offset =
      Bitvector.BMul (Some Bitvector.bpf_size_byte, ipv4_ihl_value,
        Bitvector.Const (Some Bitvector.bpf_size_byte, 4)) in
(*
    let tcp_header_start =
      Bitvector.BPlus (Some Bitvector.bpf_size_byte, ipv4_payload_start_offset,
        Bitvector.Const (Some Bitvector.bpf_size_byte, 14 + !packet_offset))
      |> Bitvector_sizing.ensure_width Bitvector.bpf_size_word in

    let tcp_header_start =
      Bitvector.BPlus (Some Bitvector.bpf_size_byte, ipv4_payload_start_offset,
        Bitvector.Const (Some Bitvector.bpf_size_byte, 14 + !packet_offset))
      |> Bitvector_sizing.ensure_width Bitvector.bpf_size_word in
*)
    let tcp_header_start =
      ipv4_payload_start_offset
      |> Bitvector_sizing.ensure_width Bitvector.bpf_size_word in

    Bitvector.BPlus (Some Bitvector.bpf_size_word, tcp_header_start, tcp_offset)

(*TODO could have a separate function that maps the "value" type.
       Currently this mapping is being done in translate_to_smt*)

let translate_to_smt (prim : primitive) : Bitvector.bv_formula =
  match prim with
  | ((Some Ether, None (*FIXME Some Src -- must be None?*), Some Proto), Escaped_String "arp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
     "(packet_proj_16 (_ bv" ^
     string_of_int (12 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 16, 0x0806) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))

  | ((Some Ip, None, None), Nothing) (*FIXME how are we coming across this pattern?*)
  | ((Some Ether, None, Some Proto), Escaped_String "ip") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
     "(packet_proj_16 (_ bv" ^
     string_of_int (12 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 16, 0x0800) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ether, None, Some Proto), Escaped_String "ip6") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
     "(packet_proj_16 (_ bv" ^
     string_of_int (12 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 16, 0x86dd) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ether, None, Some Proto), Escaped_String "rarp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
     "(packet_proj_16 (_ bv" ^
     string_of_int (12 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 16, 0x8035) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ether, None, Some Proto), Escaped_String "mpls") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
     "(packet_proj_16 (_ bv" ^
     string_of_int (12 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 16, 0x8847) in
    (*FIXME increment packet_offset*)
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ether, None, Some Proto), Escaped_String "vlan") ->
      let generate_eq (proto_num : int) =
        let lhs = Bitvector.Hole (Some Bitvector.bpf_size_halfword,
         "(packet_proj_16 (_ bv" ^
         string_of_int (12 + !packet_offset) ^
         " 32))") in
        let rhs = Bitvector.Const (Some Bitvector.bpf_size_halfword, proto_num) in
        Bitvector.Atom (Bitvector.Eq (lhs, rhs)) in
      let result = Bitvector.Or [generate_eq 0x8100; generate_eq 0x88a8;
         generate_eq 0x9100] in
      packet_offset := 4 + !packet_offset;
      result
  | ((Some Ip, None, Some Proto), Escaped_String "icmp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (23 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x01) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip, None, Some Proto), Escaped_String "tcp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (23 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x06) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip, None, Some Proto), Escaped_String "udp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (23 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x11) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip, None, Some Proto), Escaped_String "sctp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (23 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x84) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip, None, Some Proto), Escaped_String "icmp6") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (23 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x3a) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))

  | ((Some Ip6, None, Some Proto), Escaped_String "icmp6") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (20 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x3a) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip6, None, Some Proto), Escaped_String "tcp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (20 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x06) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip6, None, Some Proto), Escaped_String "udp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (20 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x11) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))
  | ((Some Ip6, None, Some Proto), Escaped_String "sctp") ->
    let lhs = Bitvector.Hole (Some Bitvector.bpf_size_byte,
     "(packet_proj_8 (_ bv" ^
     string_of_int (20 + !packet_offset) ^
     " 32))") in
    let rhs = Bitvector.Const (Some 8, 0x84) in
    Bitvector.Atom (Bitvector.Eq (lhs, rhs))

  | ((Some Tcp, Some Src, Some Port_type), Port s) ->
      (*FIXME this is specialised for IPv4, based on the assumed offsets*)
      (*FIXME this overlaps with tcp_array_extract*)
    let ipv4_is_zero_fragment =
      Bitvector.Eq
        (Bitvector.BAnd (Some Bitvector.bpf_size_halfword,
           Bitvector.Hole (Some Bitvector.bpf_size_halfword,
              "(packet_proj_16 (_ bv" ^
              string_of_int (20 + !packet_offset) ^
              " 32))"),
           Bitvector.Const (Some Bitvector.bpf_size_halfword, 0x1fff)),
         Bitvector.Const (Some Bitvector.bpf_size_halfword, 0)) in

    let ipv4_ihl_value =
      Bitvector.BAnd (Some Bitvector.bpf_size_byte,
        Bitvector.Hole (Some Bitvector.bpf_size_byte,
           "(packet_proj_8 (_ bv" ^
           string_of_int (14 + !packet_offset) ^
           " 32))"),
        Bitvector.Const (Some Bitvector.bpf_size_byte, 0xf)) in

    let ipv4_payload_start_offset =
      Bitvector.BMul (Some Bitvector.bpf_size_byte, ipv4_ihl_value,
        Bitvector.Const (Some Bitvector.bpf_size_byte, 4)) in

    let tcp_header_start =
      Bitvector.BPlus (Some Bitvector.bpf_size_byte, ipv4_payload_start_offset,
        Bitvector.Const (Some Bitvector.bpf_size_byte, 14 + !packet_offset))
      |> Bitvector_sizing.ensure_width Bitvector.bpf_size_word in

    let tcp_src_port =
      Bitvector.Hole (Some Bitvector.bpf_size_halfword,
         "(packet_proj_16 " ^
         Bitvector.string_of_bv_exp tcp_header_start ^
         ")") in

    let tcp_src_port_443 (*FIXME const*) =
      Bitvector.Eq
       (tcp_src_port,
        Bitvector.Const (Some Bitvector.bpf_size_halfword, 0x1bb(*FIXME const*))) in

    Bitvector.And [Bitvector.Atom ipv4_is_zero_fragment; Bitvector.Atom tcp_src_port_443]

  | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Not matched prim: " ^ Pcap_syntax_aux.string_of_primitive prim)
