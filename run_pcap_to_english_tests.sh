#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt pretty printer.
# Marelle Leon, April 2023

# FIXME sensitive to the directory in which this is run
tests/pcap_to_english_regression.sh "./caper.byte -1stdin -q -engl-out -not_expand" tests/disambiguate_test.sh
