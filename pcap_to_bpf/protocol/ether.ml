(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ether protocol
*)

open Inst
open Util
open Headers

let ether_type = function
  | "ip" -> 0x800
  | "ip6" -> 0x86dd
  | "arp" -> 0x806
  | "rarp" -> 0x8035
  | "mpls" -> 0x8847
  | _ -> abort_bpf_gen "unsupported protocol over ether"

let ether_mpls_type = function
  | "ip" -> 0x40
  | "ip6" -> 0x60
  | _ -> abort_bpf_gen "unsupported protocol over mpls"

let ether_proto (protocol : string) (headers : packet_headers): sock_filter option =
  match headers with
  | Nil -> Some {
      code = [Ldh (Off 12)];
      cond = Jeq (Hexj (ether_type protocol));
      jt = ret_true;
      jf = ret_false;
    }
  | Frame (_, l2_5) | Packet (_, l2_5, _) | Segment (_, l2_5, _, _) ->
    if have_mpls headers then
      let sf1 = Some {
        code = [
          Ldb (Off (14 + List.length l2_5 * 4 - 2));
          And (Hexj 0x1);
        ];
        cond = Jeq (Hexj 0x1);
        jt = ret_true;
        jf = ret_false;
      } in
      let sf2 = Some {
        code = [
          Ldb (Off (14 + List.length l2_5 * 4));
          And (Hexj 0xf0);
        ];
        cond = Jeq (Hexj (ether_mpls_type protocol));
        jt = ret_true;
        jf = ret_false;
      } in
      conjoin sf1 sf2
    else
      Some {
        code = [Ldh (Off (12 + List.length l2_5 * 4))];
        cond = Jeq (Hexj (ether_type protocol));
        jt = ret_true;
        jf = ret_false;
      }

let ether_braodcast () : sock_filter option =
  let braodcast_mac = Some {
    code = [Ld (Off 2)];
    cond = Jeq (Hexj 0xffffffff);
    jt = ret_true;
    jf = ret_false;
  } in
  let broadcast_mac2 = Some {
    code = [Ldh (Off 0)];
    cond = Jeq (Hexj 0xffff);
    jt = ret_true;
    jf = ret_false;
  } in
  conjoin braodcast_mac broadcast_mac2

let ether_multicast () : sock_filter option =
  Some {
    code = [Ldb (Off 0)];
    cond = Jeq (Hexj 0x1);
    jt = ret_true;
    jf = ret_false;
  }