(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: mpls
*)

open Inst
open Headers
open Util

let is_mpls (headers : packet_headers) : sock_filter option =
  Some {
    code = [Ldh (Off (get_protocol_size (get_l2 headers) + get_l2_5_size headers - 6))];
    cond = Jeq (Hexj 0x8847);
    jt = ret_true;
    jf = ret_false;
  }
let is_bottom (headers : packet_headers) : sock_filter option =
  Some {
    code = [Ldb (Off (get_protocol_size (get_l2 headers) + get_l2_5_size headers - 6))];
    cond = Jset (Hexj 0x1);
    jt = ret_true;
    jf = ret_false;
  }

let rec mpls_val_shift (mpls_val : int) : int =
  Int.shift_left mpls_val 12

let mpls_to_sock_filter (mpls_info : string list) (headers : packet_headers) : sock_filter option =
  let l2_5_header = get_l2_5 headers in
  match mpls_info, l2_5_header with
  | [], L2_5 Mpls :: [] -> is_mpls headers
  | [], L2_5 Mpls :: L2_5 Vlan :: l2_5 -> is_mpls headers
  | [], L2_5 Mpls :: L2_5 Mpls :: l2_5 -> is_bottom headers
  | [mpls_val], L2_5 Mpls :: [] | [mpls_val], L2_5 Mpls :: L2_5 Vlan :: _ ->
    let _mpls_val = mpls_val_shift (int_of_string mpls_val) in
    let sf = Some {
      code = [
        Ld (Off (get_protocol_size (get_l2 headers) + get_l2_5_size headers - 4));
        And (Hexj 0xfffff000)
      ];
      cond = Jeq (Hexj (_mpls_val));
      jt = ret_true;
      jf = ret_false
    } in
    conjoin (is_mpls headers) sf
  | [mpls_val], L2_5 Mpls :: L2_5 Mpls :: l2_5 ->
    let _mpls_val = mpls_val_shift (int_of_string mpls_val) in
    let sf = Some {code = [
      Ld (Off (get_protocol_size (get_l2 headers) + get_l2_5_size headers - 4));
      And (Hexj 0xfffff000)];
      cond = Jeq (Hexj (_mpls_val));
      jt = ret_true;
      jf = ret_false
    }in
    conjoin (negate (is_bottom headers)) sf
